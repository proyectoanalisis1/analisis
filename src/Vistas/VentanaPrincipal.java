/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas;

import algoritmos.Algoritmos;
import algoritmos.InsertionSort;
import algoritmos.MergeSort;
import algoritmos.QuickSort;
import algoritmos.StoogeSort;
import java.io.File;
import java.util.LinkedList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import manipulacionDatos.EscrituraArchivo;
import manipulacionDatos.FileFormat;
import manipulacionDatos.LecturaArchivo;
import operacionesAlgoritmo.OperacionesAlgoritmos;

/**
 *
 * @author Renzo Loor
 */
public class VentanaPrincipal {

    private Pane root= new Pane();
    private final String sDirectorio = "src/recursos/Files/";
    //atributos necesarios para ordenar
    public static String archivo1 = "datosGenerados.txt";
    Alert alerta = new Alert(Alert.AlertType.WARNING);
    ;
    public static int Maximo = 5000;
    TextField texto;
    public static EscrituraArchivo escritura = new EscrituraArchivo(archivo1);
    
    public static LecturaArchivo lectura;

    private LineChart linechart;//plano para graficar
    private NumberAxis xAxis;//eje x
    private NumberAxis yAxis;//eje y 
    private XYChart.Series serieInsertionSort = new XYChart.Series();
    private XYChart.Series serieMergeSort = new XYChart.Series();
    private XYChart.Series serieQuickSort = new XYChart.Series();
    private XYChart.Series serieStoogeSort = new XYChart.Series();
    
    /*private XYChart.Series maxInsertionSort = new XYChart.Series();
    private XYChart.Series maxMergeSort = new XYChart.Series();
    private XYChart.Series maxQuickSort = new XYChart.Series();
    private XYChart.Series maxStoogeSort = new XYChart.Series();*/
    
    
    private int divisiones=20;
    public VentanaPrincipal() {
        organizador();
    }

    public void organizador() {
        
        //Backgorund Images
        Image opened=new Image(getClass().getResourceAsStream("/recursos/Background/Opened.png"));
        Image closed=new Image(getClass().getResourceAsStream("/recursos/Background/Closed.png"));
        ImageView arrow=new ImageView(closed);                
        arrow.fitHeightProperty().bind(root.heightProperty());
        
        //Position that hide elements
        int x=-250;
        
        //Button: Start
        Button btn = new Button();
        btn.setText("START");
        btn.setStyle("-fx-font: 24 arial;");
        btn.layoutXProperty().bind(root.widthProperty().subtract(150));
        btn.layoutYProperty().bind(root.heightProperty().subtract(70));
        //Button: Clear
        Button btnClr = new Button();
        btnClr.setText("CLEAR");
        btnClr.setStyle("-fx-font: 24 arial;");
        btnClr.layoutXProperty().bind(root.widthProperty().subtract(150));
        btnClr.layoutYProperty().bind(root.heightProperty().divide(3).subtract(70));
        btnClr.setDisable(true);
        //Button: Save
        Button btnSv = new Button();
        btnSv.setText("SAVE");
        btnSv.setStyle("-fx-font: 24 arial;");
        btnSv.layoutXProperty().bind(root.widthProperty().subtract(150));
        btnSv.layoutYProperty().bind(root.heightProperty().divide(3));
        btnSv.setDisable(true);
        //Button: Generate RamdomFile
        Button btnRd = new Button();
        btnSv.setText("SAVE");
        btnSv.setStyle("-fx-font: 24 arial;");
        
        btnSv.setDisable(true);
        
        //CheckBoxs: Creation
        CheckBox chb1 = new CheckBox("InsertionSort");
        CheckBox chb2 = new CheckBox("MergeSort");
        CheckBox chb3 = new CheckBox("QuickSort");
        CheckBox chb4 = new CheckBox("StoogeSort");
        //CheckBox: Style
        chb1.setStyle("-fx-font: 14 arial; -fx-base: #000000;");        
        chb2.setStyle("-fx-font: 14 arial; -fx-base: #000000;");        
        chb3.setStyle("-fx-font: 14 arial; -fx-base: #000000;");        
        chb4.setStyle("-fx-font: 14 arial; -fx-base: #000000;");
        //CheckBox: Position
        chb1.setLayoutX(x);
        chb2.setLayoutX(x);
        chb3.setLayoutX(x);
        chb4.setLayoutX(x);
        chb1.setLayoutY(50);
        chb2.setLayoutY(80);
        chb3.setLayoutY(110);
        chb4.setLayoutY(140);
        chb1.selectedProperty().set(true);
        chb2.selectedProperty().set(true);
        chb3.selectedProperty().set(true);
        chb4.selectedProperty().set(true);
        
        //ComboBox
        ComboBox comboArchivo = new ComboBox();
        comboArchivo.setValue("archivo1.txt");
        comboArchivo.setStyle("-fx-font: 14 arial;");
        comboArchivo.setPromptText("Seleccione un Archivo");
        comboArchivo.setLayoutY(210);
        comboArchivo.setLayoutX(x);
        
        //Label
        Label label1 = new Label("Algoritmos de Ordenamiento: ");
        Label label2 = new Label("Archivo de Datos a Procesar: ");
        Label label3 = new Label("Cantidad de Datos a procesar: ");
        //Label:Style and Position
        label1.setStyle("-fx-font: 16 arial;-fx-base: #000000;-fx-font-weight: bold;");
        label1.setLayoutY(20);        
        label1.setLayoutX(x);
        
        label2.setStyle("-fx-font: 16 arial;-fx-base: #000000;-fx-font-weight: bold;");
        label2.setLayoutY(180);
        label2.setLayoutX(x);
        
        label3.setStyle("-fx-font: 16 arial;-fx-base: #000000;-fx-font-weight: bold;");
        label3.setLayoutY(410);
        label3.setLayoutX(x);
        
        //Textfield
        texto = new TextField();
        texto.setStyle("-fx-font: 14 arial;");
        texto.setLayoutY(435);
        texto.setLayoutX(x);
        texto.setText("250");
        texto.setPrefSize(50, 20);

        
        
        //Background's Event
        EventHandler<MouseEvent> eventhandler =new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent event) {                                
                if(arrow.getImage().equals(closed)){
                    chb1.setLayoutX(50);
                    chb2.setLayoutX(50);
                    chb3.setLayoutX(50);
                    chb4.setLayoutX(50);
                    comboArchivo.setLayoutX(50);
                    label1.setLayoutX(20);
                    label2.setLayoutX(20);
                    label3.setLayoutX(20);
                    texto.setLayoutX(50);                    
                    arrow.setImage(opened);
                }else{
                    chb1.setLayoutX(-250);
                    chb2.setLayoutX(-250);
                    chb3.setLayoutX(-250);
                    chb4.setLayoutX(-250);
                    comboArchivo.setLayoutX(-250);
                    label1.setLayoutX(-250);
                    label2.setLayoutX(-250);
                    label3.setLayoutX(-250);
                    texto.setLayoutX(-250);                    
                    arrow.setImage(closed);
                }
            }
            
        };
        arrow.addEventHandler(MouseEvent.MOUSE_CLICKED, eventhandler);
        
        //llenar el combobox on los archivos disponibles
        llenarCombo(comboArchivo);
        
        //Event from Button START
        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (comboArchivo.getValue() != null) {
                    clear();
                    
                    boolean b=ordenar(chb1, chb2, chb3, chb4,comboArchivo.getValue().toString());
                    if(b){
                        btnClr.setDisable(false);
                        btnSv.setDisable(false);
                    }
                }else{
                    alerta.setTitle("Error");
                    alerta.setHeaderText("Error de Selección");
                    alerta.setContentText("No ha seleccionado ningun archivo para ordenar");
                    alerta.showAndWait();
                }
            }
        });
        
        //Event from Button CLEAR
        btnClr.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                clear();
                btnClr.setDisable(true);
                btnSv.setDisable(true);
            }
        });
        
        //Event from Button SAVE
        btnSv.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                save(chb1,chb2,chb3,chb4);
                btnSv.setDisable(true);                
            }
        });

        //Initializing LineChart
        obtenerEscalaEjes(Integer.parseInt("250"));
        reajustarGrafico();
        root.getChildren().add(linechart);                        
        
        //Add all the nodes to the Pane
        root.getChildren().addAll(btn,btnClr,btnSv,arrow,chb1,chb2,chb3,chb4,comboArchivo,label1,label2,label3,texto);
        
        
    }
    
/**
 * Método que relaciona los diferentes algoritmos con la interfaz gráfica
 * @param chb1 CheckBox InsertionSort
 * @param chb2 CheckBox MergeSort
 * @param chb3 CheckBox QuickSort
 * @param chb4 CheckBox StoogeSort
 */
    public boolean ordenar(CheckBox chb1, CheckBox chb2, CheckBox chb3, CheckBox chb4, String archivo) {
        
        if(texto.getText().isEmpty()){
            alerta = new Alert(Alert.AlertType.ERROR);
            alerta.setTitle("Error");
            alerta.setHeaderText("Cantidad de datos vacío");
            alerta.setContentText("Por favor Ingrese datos númericos");
            alerta.showAndWait();
            return false;
        }else if(!esNumero(texto.getText())){
            alerta = new Alert(Alert.AlertType.ERROR);
            alerta.setTitle("Error");
            alerta.setHeaderText("Error en ingreso de Datos");
            alerta.setContentText("Por favor Ingrese datos númericos");
            alerta.showAndWait();
            return false;
        }
        else if (chb1.selectedProperty().get() == false && chb2.selectedProperty().get() == false && chb3.selectedProperty().get() == false && chb4.selectedProperty().get() == false) {
            alerta.setTitle("Error");
            alerta.setHeaderText("Error de Selección");
            alerta.setContentText("No ha seleccionado ningun método de ordenamiento");
            alerta.showAndWait();
            return false;
        }
        
        
        int total=Integer.parseInt(texto.getText());
        if(total>4000 || total<divisiones){
            return false;
        }
        
        reajustarEscala(total,chb4);
        
        lectura=new LecturaArchivo(archivo);
        List<Integer> listaDatos =lectura.listaDatos(total);
        
        if (chb1.selectedProperty().get() == true) {
            serieInsertionSort.setName("Insertion Sort");//etiqueta del grafico            
            serieInsertionSort.getData().add(new XYChart.Data(0, 0));            
            llenarChart(serieInsertionSort,listaDatos,new InsertionSort());
        }
        if (chb2.selectedProperty().get() == true) {
            serieMergeSort.setName("Merge Sort");//etiqueta del grafico
            serieMergeSort.getData().add(new XYChart.Data(0, 0));            
            llenarChart(serieMergeSort,listaDatos,new MergeSort());
        }
        if (chb3.selectedProperty().get() == true) {
            serieQuickSort.setName("Quick Sort");//etiqueta del grafico            
            serieQuickSort.getData().add(new XYChart.Data(0, 0));            
            llenarChart(serieQuickSort,listaDatos,new QuickSort());
        }
        if (chb4.selectedProperty().get() == true) {
            serieStoogeSort.setName("Stooge Sort");//etiqueta del grafico
            serieStoogeSort.getData().add(new XYChart.Data(0, 0));
            llenarChart(serieStoogeSort,listaDatos,new StoogeSort());
        }
        return true;
    }


    private void clear(){
        serieInsertionSort.getData().clear();
        serieMergeSort.getData().clear();
        serieQuickSort.getData().clear();
        serieStoogeSort.getData().clear();

    }
    
    
    private void save(CheckBox chb1, CheckBox chb2, CheckBox chb3, CheckBox chb4){
        int space=20;
        List<String> chbox=new LinkedList<>();
        chbox.add("n");
        if(chb1.isSelected())
            chbox.add(chb1.getText());
        if(chb2.isSelected())
            chbox.add(chb2.getText());
        if(chb3.isSelected())
            chbox.add(chb3.getText());
        if(chb4.isSelected())
            chbox.add(chb4.getText());
        
        EscrituraArchivo e=new EscrituraArchivo("Resumen.txt");
        for(String c:chbox)
            e.escribir(FileFormat.format(c, space));
        e.escribir("\n");
        
        int total=Integer.parseInt(texto.getText());
        int div=total/divisiones;
        int p=div;
        int l=1;
        String write="";
        while(l<=divisiones){
            if(p>total)
                p=total;
            e.escribir(FileFormat.format(String.valueOf(p), space));
            if(chb1.isSelected()){
                write=serieInsertionSort.getData().get(l).toString().split(",")[1];
                e.escribir(FileFormat.format(write, space));
            }
            if(chb2.isSelected()){
                write=serieMergeSort.getData().get(l).toString().split(",")[1];
                e.escribir(FileFormat.format(write, space));
            }
            if(chb3.isSelected()){
                write=serieQuickSort.getData().get(l).toString().split(",")[1];
                e.escribir(FileFormat.format(write, space));
            }
            if(chb4.isSelected()){
                write=serieStoogeSort.getData().get(l).toString().split(",")[1];
                e.escribir(FileFormat.format(write, space));
            }
            e.escribir("\n");
            l++;
            p=p+div;
        }        
        e.cerrar();
        
    }
    
    public void llenarChart(XYChart.Series chart , List<Integer> listaDatos,Algoritmos alg){
        int total=listaDatos.size();
        int div=total/divisiones;
                        
        int p=div;
        int l=0;
        while(l<divisiones){
            double inicio1 = System.currentTimeMillis();
            if(p>total)
                p=total;
            alg.sort(OperacionesAlgoritmos.partition(listaDatos, p-1));
            double fin1 = System.currentTimeMillis();
            chart.getData().add(new XYChart.Data(p, fin1-inicio1));

            l++;
            p=p+div;

        }
    }
/**
 * Método para llenar el combo box con todos los archivos que existen en el directorio
 * @param combo  ComboBox de los archivos en el directorio.
  */
    public void llenarCombo(ComboBox combo) {
        File f = new File(sDirectorio);
        ObservableList<String> items = FXCollections.observableArrayList();

        if (f.exists()) {
            File[] ficheros = f.listFiles();

            for (File fichero : ficheros) {
                items.add(fichero.getName());
            }

            combo.setItems(items);
        } else {
            System.out.println("Error directorio no existe.");
        }
    }

    private static boolean esNumero(String cadena) {
        boolean resultado;
        try {
            Integer.parseInt(cadena);
            resultado = true;
        } catch (NumberFormatException excepcion) {
            resultado = false;
        }
        return resultado;
    }
    
    private void obtenerEscalaEjes(int cantidad) {
        xAxis = new NumberAxis(0, cantidad, cantidad/divisiones);
        xAxis.setLabel("n");
        yAxis = new NumberAxis(0, cantidad /10, (cantidad /10)/divisiones);
        yAxis.setLabel("T(n)");
        linechart = new LineChart(xAxis, yAxis);

    }

     private void reajustarEscala(int cantidad,CheckBox chb4) {
        xAxis.setUpperBound(cantidad);
        xAxis.setTickUnit(cantidad / divisiones);
        xAxis.setLabel("n");
        if (chb4.isSelected()) {
            yAxis.setUpperBound(cantidad / 8);
            yAxis.setTickUnit((cantidad / 8) / divisiones);
        } else {
            yAxis.setUpperBound(cantidad /100);
            yAxis.setTickUnit((cantidad /100));
        }
        yAxis.setLabel("T(n)");
        //linechart = new LineChart(xAxis, yAxis);
    }

    private void reajustarGrafico() {
        //si se desea quitar puntos de grafica linechart.setCreateSymbols(false);
        linechart.getData().addAll(serieInsertionSort,serieMergeSort,serieQuickSort,serieStoogeSort);
        
        linechart.setLayoutX(100);
        linechart.prefWidthProperty().bind(root.widthProperty().subtract(300));//se reajusta al tamaño de su contenedor ancho (pane)
        linechart.prefHeightProperty().bind(root.heightProperty());//se reajusta al tamaño de su contenedor largo (pane)
    }
    

    public Pane getRoot() {
        return root;
    }

}
