/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmos;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Public
 */
public class InsertionSort implements Algoritmos{

    @Override
    public List<Integer> sort(List<Integer> listaDesordenada) {
        List<Integer> listaOrdenada = new ArrayList<>();
        listaOrdenada.addAll(listaDesordenada);
        for (int i = 1; i < listaOrdenada.size(); i++) {
            int puntero = listaOrdenada.get(i);
            int j = i - 1;
            while (j >= 0 && listaOrdenada.get(j) > puntero) {
                listaOrdenada.set(j + 1, listaOrdenada.get(j));
                j = j - 1;
            }
            listaOrdenada.set(j + 1, puntero);
        }
        return listaOrdenada;
    }

    @Override
    public double calcular(int n) {
        return n*n*0.0001;
    }
    
}
