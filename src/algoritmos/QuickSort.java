/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmos;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Public
 */
public class QuickSort implements Algoritmos{

    @Override
    public List<Integer> sort(List<Integer> listaDesordenada) {
        List<Integer> listaOrdenada = new ArrayList<>();
        listaOrdenada.addAll(listaDesordenada);
        qSort(listaOrdenada, 0, listaOrdenada.size() - 1);
        return listaOrdenada;
    }
    
    private void qSort(List<Integer> listaDesordenada, int inicio, int fin) {
        if (inicio < fin) {
            int particion = particion(listaDesordenada, inicio, fin);
            qSort(listaDesordenada, inicio, particion - 1);
            qSort(listaDesordenada, particion + 1, fin);
        }
    }

    private int particion(List<Integer> listaDesordenada, int inicio, int fin) {
        int particion = listaDesordenada.get(fin);
        int i = inicio - 1;
        for (int j = inicio; j < fin; j++) {
            if (listaDesordenada.get(j) <= particion) {
                i++;
                int temp = listaDesordenada.get(i);
                listaDesordenada.set(i, listaDesordenada.get(j));
                listaDesordenada.set(j, temp);
            }
        }
        int temp = listaDesordenada.get(i + 1);
        listaDesordenada.set(i + 1, listaDesordenada.get(fin));
        listaDesordenada.set(fin, temp);
        return i + 1;
    }

    @Override
    public double calcular(int n) {
        return n*((Math.log(n))/(Math.log(2)))*0.0001;
    }
    
}
