/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmos;

import java.util.List;

/**
 *
 * @author Public
 */
public interface Algoritmos {
    public List<Integer> sort(List<Integer> listaDesordenada);
    public double calcular(int n);
}
