/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmos;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Public
 */
public class StoogeSort implements Algoritmos {

    @Override
    public List<Integer> sort(List<Integer> listaDesordenada) {
        List<Integer> listaOrdenada = new ArrayList<>();
        listaOrdenada.addAll(listaDesordenada);
        sSort(listaOrdenada, 0, listaOrdenada.size() - 1);
        return listaOrdenada;
    }
    
    private void sSort(List<Integer> listaDesordenada, int inicio, int fin) {
        if (inicio >= fin) {
            return;
        }
        if (listaDesordenada.get(inicio) > listaDesordenada.get(fin)) {
            int temp = listaDesordenada.get(inicio);
            listaDesordenada.set(inicio, listaDesordenada.get(fin));
            listaDesordenada.set(fin, temp);
        }
        if (fin - inicio + 1 > 2) {
            int temp = (fin - inicio + 1) / 3;
            sSort(listaDesordenada, inicio, fin - temp);
            sSort(listaDesordenada, inicio + temp, fin);
            sSort(listaDesordenada, inicio, fin - temp);
        }
    }

    @Override
    public double calcular(int n) {
        return Math.pow(n, 2.70)*0.0001;
    }
}
