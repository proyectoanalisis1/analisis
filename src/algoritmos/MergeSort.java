/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmos;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Public
 */
public class MergeSort implements Algoritmos{

    @Override
    public List<Integer> sort(List<Integer> listaDesordenada) {
        List<Integer> listaOrdenada = new ArrayList<>();
        listaOrdenada.addAll(listaDesordenada);
        mSort(listaOrdenada, 0, listaOrdenada.size() - 1);
        return listaOrdenada;
    }
    
    private void mSort(List<Integer> listaDesordenada, int inicio, int fin) {
        if (inicio < fin) {
            int m = (inicio + fin) / 2;
            mSort(listaDesordenada, inicio, m);
            mSort(listaDesordenada, m + 1, fin);
            merge(listaDesordenada, inicio, m, fin);
        }
    }
    
    private void merge(List<Integer> listaDesordenada, int l, int m, int r) {
        int n1 = m - l + 1;
        int n2 = r - m;
        List<Integer> mitadIzquierda = new ArrayList<>(n1);
        List<Integer> mitadDerecha = new ArrayList<>(n2);
        for (int i = 0; i < n1; i++) {
            mitadIzquierda.add(i, listaDesordenada.get(l + i));
        }
        for (int j = 0; j < n2; j++) {
            mitadDerecha.add(j, listaDesordenada.get(m + 1 + j));
        }
        int i = 0, j = 0, k = l;
        while (i < n1 && j < n2) {
            if (mitadIzquierda.get(i) <= mitadDerecha.get(j)) {
                listaDesordenada.set(k, mitadIzquierda.get(i));
                i++;
            } else {
                listaDesordenada.set(k, mitadDerecha.get(j));
                j++;
            }
            k++;
        }
        while (i < n1) {
            listaDesordenada.set(k, mitadIzquierda.get(i));
            i++;
            k++;
        }
        while (j < n2) {
            listaDesordenada.set(k, mitadDerecha.get(j));
            j++;
            k++;
        }
    }

    @Override
    public double calcular(int n) {
        return n*((Math.log(n))/(Math.log(2)))*0.0001;
    }
    
}
