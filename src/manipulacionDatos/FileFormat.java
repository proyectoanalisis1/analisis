/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manipulacionDatos;

/**
 *
 * @author Public
 */
public class FileFormat {
    
    public static String format(String word,int space){
        if(word.isEmpty() || space<=0)
            return null;
        int min=word.length();
        StringBuilder sb=new StringBuilder();
        sb.append(word);
        for(int i=0;i<space-min;i++)
            sb.append(' ');
        return sb.toString();
    }
}
