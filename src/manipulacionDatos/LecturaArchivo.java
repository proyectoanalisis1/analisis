
package manipulacionDatos;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Public
 */
public class LecturaArchivo {
    
    private final String fileName;
    private String line;
    private String nLine;
    private BufferedReader inputStream;
    
    public LecturaArchivo(String fileName){
        this.fileName="src/recursos/Files/"+fileName;
        line="";
        beginFile();
        
    }
    
    private void beginFile(){
        try
        { 
            inputStream = new BufferedReader(new FileReader(fileName));
            nLine=inputStream.readLine();  
        }
        catch(FileNotFoundException e)
        {
          System.out.println("Archivo no encontrado.");
        }catch(IOException e)
        {
            System.out.println("Error leyendo el archivo.");
        }    
    }
    
    public String leer(){
        try
        { 
            line=nLine;
            nLine=inputStream.readLine();
            return line;
        }catch(IOException e)
        {
           System.out.println("Error leyendo el archivo.");
        } 
        return null;
    }
    
    public boolean has(){
        return nLine!=null;
    }
    public void cerrar(){
        try { 
            inputStream.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }  
    
    public List<Integer> listaAleatoria(){
        List<Integer> listaAleatoria = new ArrayList<>();
        leer();
        while (has()){
            int linea = Integer.parseInt(leer());
            listaAleatoria.add(linea);
        }
        cerrar();
        return listaAleatoria;
    } 
    
    public List<Integer> listaDatos(int stop){
        List<Integer> listaDatos = new ArrayList<>();        
        int i=0;
        while (has() && i<stop){
            int linea = Integer.parseInt(leer());
            listaDatos.add(linea);
            i++;
        }        
        cerrar();
        return listaDatos;
    }
    
}
