/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manipulacionDatos;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.List;

/**
 *
 * @author Public
 */
public class EscrituraArchivo {
    private PrintWriter file;
    private final String fileName;
    
    public EscrituraArchivo(String name){
        this.fileName="src/recursos/Result/"+name;
        beginFile();
    }

    private void beginFile(){        
        try
        { 
            file = new PrintWriter(new FileOutputStream(fileName));
            
        }catch(FileNotFoundException e)
        {
            System.out.println("Error abriendo el archivo out.txt."+ e.getMessage());            
        }
        
    }
    
    public void escribir(String texto){
        file.print(texto);
    }
    
    public void escribirColeccion(List<String> lista){
        int c=0;
        for(String i:lista){            
            escribir(i);
            c++;
            if(c<lista.size())
                escribir("\n");
        }
    }
    
    public void cerrar(){
        file.close();
    }   
    
    public void generarDatosAleatorios(int Maximo){
        for (int i = 0; i < Maximo; i++) {
            Integer numero = (int) (Math.random() * 500)+1;
            if(i==Maximo-1){
               escribir(numero.toString());
            }else{
               escribir(numero.toString()+"\n"); 
            }
        }
        cerrar();
    }
    
}

